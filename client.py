#!/usr/bin/python3
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys


try:
   SERVER = sys.argv[1]
   PORT = int(sys.argv[2])
   REGISTER = sys.argv[3]
   USER = sys.argv[4]
   EXPIRE = sys.argv[5]

except ValueError:
   sys.exit("Usage: client.py ip puerto register sip_address expires_value")
if REGISTER != "register":
    print("Usage: client.py ip puerto register sip_address expires_value")
    exit()

with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.connect((SERVER, PORT))
    print("Enviando:", USER)
    my_socket.send(b"REGISTER sip:" + bytes(USER, 'utf-8') + b" SIP/2.0" + b"\r\n\r\n")
    my_socket.send(b"Expires: " + bytes(EXPIRE, 'utf-8') + b"\r\n\r\n")
    data = my_socket.recv(1024)
    print('Recibido -- ', data.decode('utf-8'))

print("Socket terminado.")
