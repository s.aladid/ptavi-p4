#!/usr/bin/python3
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import json
import time

class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    info = {}

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        self.json2registered()
        linea = self.rfile.read().decode("utf-8").split()
        if linea[0] == "REGISTER":
            self.info["address"] = linea[1]
            self.info["IP"] = self.client_address[0]
            self.register2json()
            self.wfile.write(b"SIP/2.0 200 OK" + b"\r\n\r\n")
        if linea[0] == "Expires:":
            if linea[1] == str(0):
                try:
                    del self.info["address"]
                    del self.info["IP"]
                    del self.info["expires"]
                    self.wfile.write(b"SIP/2.0 200 OK" + b"\r\n\r\n")
                    self.register2json()
                except KeyError:
                    print("Key not found")
            elif linea[1] != str(0):
                tiempo = int(linea[1]) + int(time.time())
                tiempo_hoy = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime(tiempo))
                self.info["expires"] = tiempo_hoy
                self.register2json()

    def register2json(self):
        f = open("registered.json", "w+")
        json.dump(self.info, f, indent=4)
        f.close()

    def json2registered(self):
       try:
           with open("registered.json") as f:
               self.info = json.load(f)
       except FileNotFoundError:
           self.info = {}

if __name__ == "__main__":
    try:
       PORT = int(sys.argv[1])
    except ValueError:
       sys.exit("Tienes que introducir un puerto")
    serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)
    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
